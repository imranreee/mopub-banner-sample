import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.net.URL;
import java.util.concurrent.TimeUnit;

/**
 * Created by: Al Imran on 28/10/2018.
 * Email: imranreee@gmail.com
 **/

public class MoPubSampleApp {
    protected WebDriver driver;
    DesiredCapabilities dc = new DesiredCapabilities();

    @BeforeClass
    public void setUp() throws Exception {
        dc.setCapability("deviceName", "OnePlusOne");
        dc.setCapability("deviceVersion", "8.1");
        dc.setCapability("platformName", "Android");
        dc.setCapability("appPackage", "com.mopub.simpleadsdemo");
        dc.setCapability("appActivity", "com.mopub.simpleadsdemo.MoPubSampleActivity");

        dc.setCapability("autoGrantPermissions", "true");
        dc.setCapability("unicodeKeyboard", "true");
        dc.setCapability("resetKeyboard", "true");
        dc.setCapability("noReset", "true");
        dc.setCapability("fullReset", "false");
        

        driver = new AndroidDriver(new URL("http://0.0.0.0:4723/wd/hub"), dc);

    }

    @Test(priority = 1)
    public void findAddLink() throws Exception {
        By bannerSample = By.id("banner_description");
        By addLink = By.xpath("//*[@text=' Success! Tap to test this ad.']");
        By expectedText = By.xpath("//*[@text='Looking for MoPub resources?']");

        System.out.println("Waiting for banner sample");
        waitForVisibilityOf(bannerSample);
        driver.findElement(bannerSample).click();
        System.out.println("Found banner sample and clicked");

        System.out.println("Waiting for 10 seconds for the add link");

        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Thread.sleep(3000);
        Thread.sleep(3000);
        Thread.sleep(3000);
        Thread.sleep(1000);

        if (driver.findElements(addLink).size() > 0){
            driver.findElement(addLink).click();
            System.out.println("Add link found and clicked");

            waitForVisibilityOf(expectedText);
            driver.findElement(expectedText).click();

            String expectedTextStr = driver.findElement(expectedText).getText();
            Assert.assertEquals(expectedTextStr,"Looking for MoPub resources?");
            System.out.println("Found expected text: "+expectedTextStr);
        }else {
            System.out.println("Link not found!");
        }
    }


    @AfterClass
    public void endTest(){
        driver.quit();
    }

    protected void waitForVisibilityOf(By locator) {
        WebDriverWait wait = new WebDriverWait(driver, 60);
        wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
    }

}
